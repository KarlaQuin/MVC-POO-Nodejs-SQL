//Controlador de rutas
const fs = require('fs');
const path = require('path');
const view = path.join(__dirname, '..', '..', 'view/admin');
const ejs = require('ejs');
const querystring = require('querystring');

//importando modelos
const {Notes} = require('../../models/notes');
const notes = new Notes();
const LoginAdmin = require('../../models/admin/admin');

//Clase Admin que contiene métodos para el administrador
class Admin {
    //Página principal al cargar será el login
    index(req, res) {
        fs.readFile(path.join(view ,'login.html'), 'utf-8', (err, html) => {
            res.writeHead(200, { "Content-Type": 'text/html' });
            res.end(html);
        });
    }
    //Página de inicio una vez loggeado
    home(req, res){        
        LoginAdmin.searchUsers()
            .then((result)=>{
                fs.readFile(path.join(view ,'index.html'), 'utf-8', (err, html) => {
                    res.writeHead(200, { "Content-Type": 'text/html' });
                    const renderedHTML = ejs.render(html, { data:result });
                    res.end(renderedHTML);
                });
            })
            .catch(error=>{
                fs.readFile(path.join(view ,'index.html'), 'utf-8', (err, html) => {
                    res.writeHead(200, { "Content-Type": 'text/html' });
                    const renderedHTML = ejs.render(html, { data:[{title:"", description:"", count_notes:0}] });
                    res.end(renderedHTML);
                });
            });
    }
    //Para validar los datos del login
    login(req, res) {
        let body = '';
        req.on('data', (chunk) => {
            body += chunk;
        })

        req.on('end', () => {
            const formData = querystring.parse(body)
            console.log(formData);
            const {email, pass} = formData;
            
            LoginAdmin.login(email, pass)
            .then(()=>{
                res.writeHead(302, { 'Location': '/admin/index' });
                res.end()
            })
            .catch(()=>{
                res.writeHead(302, { 'Location': '/admin' });
                res.end()
            });
        });
    }

    newNote(req, res){
        let body = '';
        req.on('data', (chunk) => {
            body += chunk;
        })

        req.on('end', () => {
            const formData = querystring.parse(body)
            const {title, description} = formData;

            notes.insertNote(title, description, loginSystem.userId)
                .then(()=>{
                    res.writeHead(302, { 'Location': '/admin/index' });
                    res.end()
                })
                .catch((error)=>{
                    res.writeHead(500, { 'Location': '/admin/index' });
                    res.end()
                });
        });
    }

    editNote(id, req, res){        
        let body = '';
        req.on('data', (chunk) => {
            body += chunk;
        })

        req.on('end', () => {
            const formData = querystring.parse(body)
            console.log(formData);
            const {title, description} = formData;

            notes.editNote(id,title, description)
                .then(()=>{
                    res.writeHead(302, { 'Location': '/index' });
                    res.end()
                })
                .catch(error=>{
                    res.writeHead(500, { 'Location': '/index' });
                    res.end()
                })
        });
    }

    deleteNote(id, req, res){
        notes.deleteNote(id)
            .then(()=>{
                res.writeHead(302, { 'Location': '/index' });
                res.end()
            })
            .catch(error=>{
                res.writeHead(500, { 'Location': '/index' });
                res.end()
            })
    }
}

//Exportando la clase Router para que pueda ser utilizada en la aplicación (app.js)
exports.Admin = Admin;
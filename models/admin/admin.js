const connection = require('../../connectiondb');

//Clase que maneja el inicio de sesión del usuario
class LoginSystemAdmin {

    //Constructor que guarda la activación del usuario
    constructor() {
        this.active = false;
        this.id;
    }

    //Para iniciar sesión
    login(email, password) {
        return new Promise((resolve, reject) => {
            const query = `SELECT * FROM superuser WHERE email = ?`;
            connection.all(query, [email], (error, data) => {
                console.log(data);                
                if (error) {
                    reject(error);
                } else if (data.length === 0) {
                    console.error("Admin not found");
                    reject(false);
                } else {
                    const admin = data[0];
                    this.id = data[0].id;
                    if (admin.password !== password) {
                        console.error("Invalid password");
                        reject(false);
                    } else {
                        console.log(`admin: ${admin.email} logged`);
                        this.active = true;
                        resolve(true);
                    }
                }
            });
        });
    }
    
    //Para buscar usuarios
    searchUsers() {
        return new Promise((resolve, reject) => {
            const query = `SELECT * FROM users`;
            connection.all(query, (error, data) => {              
                if (error) {
                    reject(error);
                } 
                resolve(data);
            });
        });
    }

    //Para saber si el admin está activo
    get isActive() {
        return this.active;
    }

    get userId() {//Para saber el id del admin
        return this.id;
    }

}

const LoginAdmin = new LoginSystemAdmin();

module.exports = LoginAdmin;
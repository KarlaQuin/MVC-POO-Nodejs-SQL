const connection = require('../connectiondb');
const CryptoJS = require("crypto-js");
require('dotenv').config();

//Clase que maneja el inicio de sesión del usuario
class LoginSystem {

    //Constructor que guarda la activación del usuario
    constructor() {
        this.active = false;
        this.id;
    }

    //Método para registrarse
    register(username, email, pass) {
        const password = CryptoJS.AES.encrypt(pass, process.env.KEY).toString();
        return new Promise((resolve, reject) => {
            const query = `SELECT * FROM users WHERE username = ?`;
            connection.all(query, [username], (error, data) => {
                if (error) {
                    reject(error);
                } else if (data.length > 0) {
                    resolve({msg:"Username already exists"});
                } else {
                    const insertQuery = `INSERT INTO users (username, email, password, count_notes) VALUES (?, ?, ?, ?)`;
                    connection.run(insertQuery, [username, email, password, 0], (insertError) => {
                        if (insertError) {
                            reject(insertError);
                        } else {
                            console.log("User registered successfully");
                            resolve();
                        }
                    });
                }
            });
        });
    }

    //Para iniciar sesión
    login(email, password) {

        return new Promise((resolve, reject) => {
            const query = `SELECT * FROM users WHERE email = ?`;
            connection.all(query, [email], (error, data) => {
                if (error) {
                    reject(error);
                } else if (data.length === 0) {
                    console.error("User not found");
                    reject(false);
                } else {
                    const user = data[0];
                    this.id = data[0].id;
                    if (CryptoJS.AES.decrypt(user.password, process.env.KEY).toString(CryptoJS.enc.Utf8) !== password) {
                        console.error("Invalid password");
                        reject(false);
                    } else {
                        console.log(`User: ${user.username} logged`);
                        this.active = true;
                        resolve(true);
                    }
                }
            });
        });
    }

    //Para saber si el usuario está activo
    get isActive() {
        return this.active;
    }

    get userId() {//Para saber el id del usuario
        return this.id;
    }

}

const loginSystem = new LoginSystem();

module.exports = loginSystem;
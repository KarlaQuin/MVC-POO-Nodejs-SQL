//Modelo que maneja la base datos
const connection = require('../connectiondb');

let querys = {
    select: "SELECT n.title, n.description FROM notes n INNER JOIN users u ON n.user_id = u.id WHERE u.id = ?",
    insert: "INSERT INTO notes (title, description, user_id) VALUES (?, ?, ?)",
    update: "UPDATE notes SET title = ?, description = ? WHERE id = ?",
    update_count_notes: "UPDATE users SET count_notes = count_notes + ? WHERE id = ?",
    delete: "DELETE FROM notes WHERE id = ?"
};

//Calse para simular la base de datos para ver, insertar, borrar y editar
class Notes{
    getNotes(user_id) {
        return new Promise((resolve, reject) => {
            connection.all(querys.select, [user_id], (error, results) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(results);
                }
            });
        });
    }

    insertNote(title, description, user_id) {

        return new Promise((resolve, reject) => {
            connection.run(querys.insert, [title, description, user_id], (error) => {
                if (error) {
                    reject(error);
                } else {
                    connection.run(querys.update_count_notes, [1, user_id], (error) => {
                        if (error) {
                            reject(error);
                        } else {
                            resolve();
                        }
                    });
                }
            });
        });
    }

    deleteNote(id, user_id) {
        return new Promise((resolve, reject) => {
            connection.run(querys.delete, [id], (error) => {
                if (error) {
                    reject(error);
                } else {
                    connection.run(querys.update_count_notes, [-1, user_id], (error) => {
                        if (error) {
                            reject(error);
                        } else {
                            resolve();
                        }
                    });
                }
            });
        });
    }

    editNote(id, newTitle, newDescription) {
        return new Promise((resolve, reject) => {
            connection.run(querys.update, [newTitle, newDescription, id], (error) => {
                if (error) {
                    reject(error);
                } else {
                    resolve();
                }
            });
        });
    }
}

exports.Notes = Notes;
//Create connection to mysql
const sqlite = require('sqlite3').verbose();
const path = require('path');
const fs = require('fs');

const connection = new sqlite.Database(path.join(__dirname, "data.db"), () => {
    console.log("Connect with SQLite 3");
    connection.run(`CREATE TABLE IF NOT EXISTS notes (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        title VARCHAR(255) NOT NULL,
        description TEXT NOT NULL,
        user_id INTEGER,
        FOREIGN KEY(user_id) REFERENCES users(id)
    );`);

    connection.run(`CREATE TABLE IF NOT EXISTS users (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        username VARCHAR(255) NOT NULL,
        email VARCHAR(255) NOT NULL,
        password VARCHAR(255) NOT NULL,
        count_notes INTEGER
    );`);

    connection.run(`CREATE TABLE IF NOT EXISTS superuser (
        id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
        email VARCHAR(255) NOT NULL,
        password VARCHAR(255) NOT NULL
    );`);

    connection.get(`SELECT COUNT(*) AS count FROM superuser`, [], (err, row) => {
        if (err) {
            console.error(err);
            return;
        }
        
        if (row.count == 0) {
            connection.run(`INSERT INTO superuser (email, password) VALUES (?, ?)`, ["admin@notes.com", "admin"], (err) => {
                if (err) {
                    console.error(err);
                    return;
                }
                console.log("Admin Creado");
            });
        }
    });
});

module.exports = connection;